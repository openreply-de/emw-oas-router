'use strict';

class ResolverHandlerError extends Error {
  constructor(message) {
    super();

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    Error.captureStackTrace(this, ResolverHandlerError);

    this.name = 'ResolverHandlerError';
  }
}

module.exports = ResolverHandlerError;
