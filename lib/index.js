/**
 * @file exposes the router and the resolver
 * @author Florian Schaper <f.schaper@reply.de>
 * @copyright Florian Schaper 2018, MIT License
 */

'use strict';

module.exports = require('./router');
module.exports.Resolver = require('./resolver');
