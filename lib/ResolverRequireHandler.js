'use strict';

class ResolverRequireError extends Error {
  constructor(message, originalError) {
    super();

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    Error.captureStackTrace(this, ResolverRequireError);

    this.name = 'ResolverRequireError';
    this.originError = originalError;
  }
}

module.exports = ResolverRequireError;
