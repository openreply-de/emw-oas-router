# openapi 3.x express router middleware

```javascript
const router = require('@openreply/emw-oas-router');
const express = require('express');

const app = express();

// eslint-disable-next-line global-require
app.use(router({ specification: require('./specification/api.json') }));

app.listen(3000);
```
