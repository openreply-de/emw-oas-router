const express = require('express');
const router = require('../lib/router');

const app = express();

const logger = {
  onError: ({ method, path }) => {
    // eslint-disable-next-line no-console
    console.log(`failed to add route for ${method} ${path} - continuing`);
    return true;
  },
  // eslint-disable-next-line no-console
  onBeforeRouteMethodAdded: ({ method, path, options }) =>
    console.log(
      `added route for ${method} ${path} ${
        options['x-controller']
          ? `using override controller ${
              options['x-controller']
            } due to specification`
          : ''
      }`
    )
};

app.use(
  router({
    // eslint-disable-next-line global-require
    specification: require('./specification/api.json'),
    onError: logger.onError,
    onBeforeRouteMethodAdded: logger.onBeforeRouteMethodAdded
  })
);

app.listen(3000);
