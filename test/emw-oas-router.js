const router = require('..');
const { expect } = require('chai');

const express = require('express');

function serverFactory({ withRouter: expressRouter }) {
  const app = new express();
  app.use(expressRouter);
  return app;
}

describe('emw-oas-router', () => {
  describe('initialization', () => {
    it('should throw if called without arguments', () => {
      expect(() => {
        router();
      }).to.throw();
    });

    it('should throw if called without a specification object', () => {
      expect(() => {
        router({});
      }).to.throw();
    });

    it('should throw if specification object is missing a paths object', () => {
      expect(() => {
        router({ specification: {} });
      }).to.throw();
    });
  });
});
