
## resolver(options) ⇒ ExpressHandler
resolved an api specification to local source files and exported method handlers

**Kind**: global function  
**Returns**: ExpressHandler - express callback handler  
**Throws**:

- Error in case that a controller or method could not be resolved


| Param | Type | Description |
| --- | --- | --- |
| options | Object |  |
| options.basePath | string | base filename to look for controllers to match with the api spec |

## router(options) ⇒ Object
initializes an express router object with routes as defined by the passed open api 3.0.x specification

**Kind**: global function  
**Returns**: Object - an express router middleware  
**Throws**:

- TypeError if an invalid configuration option has been passed to this function
- forwards additional thrown exceptions to the caller, depending on the resolver used and the configured error handler


| Param | Type | Description |
| --- | --- | --- |
| options | RouterConfiguration | configuration for the middleware. only `specification` is required if the defaults suit your needs |

## Next : function
**Kind**: global typedef  
## ExpressHandler : function
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| req | Object | the http request object |
| res | Object | the http response object |
| next | Next | a continuation function |

## Resolver ⇒ ExpressHandler
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| path | string | api path as defined in the oas specification |
| method | string | method as defined by the oas specification e.g. get, put, post, delete, head, ... |
| options | Object | additional configuration for the method under the given path |

## RouterErrorCallback : function
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| exception | Error | exception thrown by the resolver |
| path | string | path as defined by the open api specification |
| method | string | a method as defined by the open api specification for that path |
| options | Object | additional configuration options for that path and method from the open api specification |

## RouterOnBeforeRouteAddedCallback : function
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| path | string | path as defined by the open api specification |
| options | Object | additional configuration options for that path and containing all methods and their configurations |

## RouterOnBeforeMethodAddedCallback : function
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| handler | ExpressHandler | handler for the specific path and method |
| path | string | path as defined by the open api specification |
| method | string | a method as defined by the open api specification for that path |
| options | Object | additional configuration options for that path and method from the open api specification |

## RouterConfiguration : Object
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| specification | Object | the open api specification that will be used for mapping to the controllers |
| router | Object | an express router object where the routes from the specification will be attached to. you can use this e.g. to configure a base path to your api. by default a new `express.Router()` object is used. |
| resolver | Resolver | function which resolves a uri path and method to an express function handler. See the documentation for the resolver for more detail about the default behavior. |
| onError | RouterErrorCallback | callback that will be called for errors that may occur during handler lookup. defaults to a function returning `false` - which will re-throw any exception and abort the router configuration. |
| onBeforeRouteAdded | RouterOnBeforeRouteAddedCallback | callback that will be called for every route path that is to be added to the router. defaults to an `noop`. |
| onBeforeRouteMethodAdded | RouterOnBeforeMethodAddedCallback | callback that will be called for every route method that is to be added to the current router path. defaults to an `noop`. |

